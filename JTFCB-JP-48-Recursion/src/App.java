//Recursion to algorytm
// trzeba to umiec w sumie
public class App {

	public static void main(String[] args) {

		
		System.out.println(silnia(5));

	}

	// nie mam obiektu w klasie App
	// i chce uzyc metody bezposrednio z main
	// dwie linijki powyzej nie maja nic zwiazanego z rekursja
	// po prostu tak chcial

	// mozemy zrobic zeby metoda wolala sie sama
	// jak przywolujesz metode z innej metody
	// to jest problem
	// stack to czesc memory of jvm
	// stack is used for local variables
	// stack pamieta ktora metoda przywolala ktora metode
	// heap to obszar pamieci kiedy uzywasz operator new
	// tu jest stackoverflow bo jedna przywoluje druga
	// nieskonczona petla

	// za duzo recursji nie poleca sie uzywac
	/*
	 * private static void calculate(int value) { System.out.println(value);
	 * calculate(value);
	 * 
	 * }
	 */

	// gdy wpisujemy value -1 inaczej error wyglada
	// wyglada na to ze value pierwsza wartosc to 4 jak powyzej
/*	private static void calculate(int value) {
		System.out.println(value);
		calculate(value - 1);

	} */
	
	// tu usunal ten stackoverflow
	// dal petle z returnem
	// private static void calculate(int value) {
	
	// zrobil funkcje na silnie!!!
	private static int silnia(int value) {
	System.out.println(value);
	if(value==1){
		
		return 1;
	}
	return silnia(value - 1)*value;

} 
	// przydaje sie to do obliczenia silni
	// teoria prawdopodobienstwa
	
	// pamietac ze rekursje trzeba zakonczyc
	// i raczej operowac na malych liczbach
	
	// recursja to do ogarniecia tower of hanoi!!!
	// wlasciwie to ten kolorowy ma znaczenie
	// zeby ogarnac rekursje trzeba rozkminic silnie
	// i dalej metodyka roznych zadan podobna
}
